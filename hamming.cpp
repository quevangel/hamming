#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <cassert>

std::string RESET = "\e[0m";
std::string RED = "\e[31m";

struct PositionedBit
{
    unsigned position;
    bool value;
};

struct HammingEncoding
{
    std::vector<PositionedBit> check_bits;
    std::vector<PositionedBit> data_bits;
};

std::vector<bool> Bits_From_String(std::string bit_string);
std::vector<bool> Bits_From_Unsigned(unsigned n, unsigned no_bits = 0);
std::vector<bool> Bits_From_Hamming_Encoding(HammingEncoding he);

unsigned Unsigned_From_Bits(std::vector<bool> bits);

HammingEncoding Hamming_Encoding_From_Bits(std::vector<bool> hamming_bits);
HammingEncoding Hamming_Encoding_From_Data_Bits(std::vector<bool> data_bits);

void Print_Hamming_Encoding(HammingEncoding he);
unsigned No_Parity_Bits(unsigned no_data_bits);
bool Is_Power_Of_2(unsigned n);

void Program_Encode(std::vector<bool> bits);
void Program_Correct(std::vector<bool> bits);

std::ostream& operator<<(std::ostream& os, std::vector<bool> bits)
{
    //for(auto bit: bits)
    for(std::vector<bool>::iterator bit_it = bits.begin(); bit_it != bits.end(); bit_it++)
        os << *bit_it;
    return os;
}

std::ostream& operator<<(std::ostream& os, HammingEncoding he)
{
    Print_Hamming_Encoding(he);
    return os;
}

void Print_Help();

int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        Print_Help();
        return 0;
    }
    std::string operation(argv[1]);
    std::string bit_string(argv[2]);
    std::vector<bool> bits = Bits_From_String(bit_string);
    if(bit_string.size() != bits.size())
    {
        std::cout << "La cadena debe consistir solamente de 1's y 0's" << '\n';
        return 0;
    }

    if(operation == "codificar")
    {
        Program_Encode(bits);
    }
    else if(operation == "corregir")
    {
        Program_Correct(bits);
    }
    else
    {
        Print_Help();
    }

    return 0;
}

void Program_Encode(std::vector<bool> data_bits)
{
    std::cout << "Datos = " << data_bits << '\n';
    unsigned no_parity_bits = No_Parity_Bits(data_bits.size());
    std::cout << "Número de bits de paridad = " << no_parity_bits << '\n';
    HammingEncoding encoding = Hamming_Encoding_From_Data_Bits(data_bits);
    std::cout << "Bits codificados = " << encoding << '\n';
}

void Program_Correct(std::vector<bool> hamming_bits)
{
    std::cout << "corregir" << '\n';
    HammingEncoding probably_bad_he = Hamming_Encoding_From_Bits(hamming_bits);
    std::vector<bool> probably_bad_check_bits;
    std::vector<bool> probably_bad_data_bits;
    //for(auto pbit: probably_bad_he.check_bits)
    for(std::vector<PositionedBit>::iterator pbit_ptr = probably_bad_he.check_bits.begin();
            pbit_ptr != probably_bad_he.check_bits.end(); pbit_ptr++)
        probably_bad_check_bits.push_back((*pbit_ptr).value);
    //for(auto pbit: probably_bad_he.data_bits)
    for(std::vector<PositionedBit>::iterator pbit_ptr = probably_bad_he.data_bits.begin();
            pbit_ptr != probably_bad_he.data_bits.end(); pbit_ptr++)
        probably_bad_data_bits.push_back((*pbit_ptr).value);

    HammingEncoding calculated_he = Hamming_Encoding_From_Data_Bits(probably_bad_data_bits);
    std::vector<bool> calculated_check_bits;
    //for(auto pbit: calculated_he.check_bits)
    for(std::vector<PositionedBit>::iterator pbit_ptr = calculated_he.check_bits.begin();
            pbit_ptr != calculated_he.check_bits.end(); pbit_ptr++)
        calculated_check_bits.push_back((*pbit_ptr).value);

    unsigned calculated_check_bits_u = Unsigned_From_Bits(calculated_check_bits);
    unsigned probably_bad_check_bits_u = Unsigned_From_Bits(probably_bad_check_bits);
    unsigned difference = calculated_check_bits_u ^ probably_bad_check_bits_u;
    if(difference == 0)
    {
        std::cout << "La secuencia es correcta" << '\n';
        std::cout << hamming_bits << '\n';
    }
    else
    {
        std::cout << "Hay un error en el bit no." << difference << '\n';
        std::cout << "Secuencia correcta: ";
        unsigned i = 0;
        //for(auto bit: hamming_bits)
        for(std::vector<bool>::iterator bit_ptr = hamming_bits.begin();
                bit_ptr != hamming_bits.end(); bit_ptr++)
        {
            if(i + 1 == difference)
            {
                std::cout << RED << !(*bit_ptr);
            }
            else
            {
                std::cout << RESET << *bit_ptr;
            }
            i++;
        }
        std::cout << '\n';
    }
}

void Print_Help()
{
    std::cout << "Uso: ./hamming [codificar | corregir] <bits>" << '\n';
}

std::vector<bool> Bits_From_String(std::string bit_string)
{
    std::vector<bool> bits;
    //for(auto bit_char: bit_string)
    for(std::string::iterator bit_char_ptr = bit_string.begin();
            bit_char_ptr != bit_string.end(); bit_char_ptr++)
    {
        char bit_char = *bit_char_ptr;
        if(bit_char == '0' || bit_char == '1')
        {
            bits.push_back(bit_char == '1');
        }
        else
        {
            break;
        }
    }
    return bits;
}

unsigned Unsigned_From_Bits(std::vector<bool> bits)
{
    if(bits.size() == 0)
        return 0;
    unsigned lsb = bits[0]? 1: 0;
    return lsb + 2 * Unsigned_From_Bits(std::vector<bool>(bits.begin() + 1, bits.end()));
}

HammingEncoding Hamming_Encoding_From_Data_Bits(std::vector<bool> data_bits)
{
    HammingEncoding encoding;
    unsigned used_data_bits = 0;
    for(unsigned position = 1; used_data_bits < data_bits.size(); position++)
    {
        if(Is_Power_Of_2(position))
        {
            PositionedBit pbit;
            pbit.position = position;
            pbit.value = false;

            encoding.check_bits.push_back(pbit);
        }
        else
        {
            PositionedBit pbit;
            pbit.position = position;
            pbit.value = data_bits[used_data_bits];
            used_data_bits++;

            encoding.data_bits.push_back(pbit);
        }
    }

    unsigned xored_positions = 0u;
    for(unsigned i = 0; i < encoding.data_bits.size(); i++)
    {
        bool value = encoding.data_bits[i].value;
        if(value)
        {
            xored_positions ^= encoding.data_bits[i].position;
        }
    }

    std::vector<bool> check_bits = Bits_From_Unsigned(xored_positions, encoding.check_bits.size());
    for(unsigned i = 0; i < check_bits.size(); i++)
    {
        encoding.check_bits[i].value = check_bits[i];
    }

    std::cout << "bits de datos = ";
    //for(auto pbit: encoding.data_bits)
    for(std::vector<PositionedBit>::iterator pbit_ptr = encoding.data_bits.begin();
            pbit_ptr != encoding.data_bits.end(); pbit_ptr++)
    {
        PositionedBit pbit = *pbit_ptr;
        std::cout << "(" << pbit.position << ": " << pbit.value << ")";
    }
    std::cout << '\n';

    std::cout << "bits de paridad = ";
    //for(auto pbit: encoding.check_bits)
    for(std::vector<PositionedBit>::iterator pbit_ptr = encoding.check_bits.begin(); 
            pbit_ptr != encoding.check_bits.end(); pbit_ptr++)
    {
        PositionedBit pbit = *pbit_ptr;
        std::cout << "(" << pbit.position << ": " << pbit.value << ")";
    }
    std::cout << '\n';

    return encoding;
}

std::vector<bool> Bits_From_Hamming_Encoding(HammingEncoding he)
{
    std::vector<bool> bits;
    unsigned n = he.check_bits.size() + he.data_bits.size();
    unsigned check_i = 0;
    unsigned data_i = 0;
    std::cout << "total bits = " << n << '\n';
    for(unsigned i = 0; i < n; i++)
    {
        unsigned check_position = 0, data_position = 0;
        bool remains_check = check_i < he.check_bits.size();
        bool remains_data = data_i < he.data_bits.size();

        if(remains_check)
            check_position = he.check_bits[check_i].position;
        if(remains_data)
            data_position = he.data_bits[data_i].position;

        assert(remains_data || remains_check);

        if(!remains_data)
        {
            bits.push_back(he.check_bits[check_i].value);
            check_i++;
            continue;
        }
        if(!remains_check)
        {
            bits.push_back(he.data_bits[data_i].value);
            data_i++;
            continue;
        }

        if(check_position < data_position)
        {
            bits.push_back(he.check_bits[check_i].value);
            check_i++;
            continue;
        }
        else if(data_position < check_position)
        {
            bits.push_back(he.data_bits[data_i].value);
            data_i++;
            continue;
        }
        else
        {
            assert(false);
        }
    }
    return bits;
}

HammingEncoding Hamming_Encoding_From_Bits(std::vector<bool> hamming_bits)
{
    HammingEncoding encoding;
    for(unsigned i = 0; i < hamming_bits.size(); i++)
    {
        PositionedBit pbit;
        pbit.position = i + 1;
        pbit.value = hamming_bits[i];
        if(Is_Power_Of_2(i + 1))
            encoding.check_bits.push_back(pbit);
        else
            encoding.data_bits.push_back(pbit);
    }
    return encoding;
}

void Print_Hamming_Encoding(HammingEncoding he)
{
    unsigned n = he.check_bits.size() + he.data_bits.size();
    unsigned check_i = 0;
    unsigned data_i = 0;
    for(unsigned i = 0; i < n; i++)
    {
        unsigned check_position = 0, data_position = 0;
        bool remains_check = check_i < he.check_bits.size();
        bool remains_data = data_i < he.data_bits.size();

        if(remains_check)
            check_position = he.check_bits[check_i].position;
        if(remains_data)
            data_position = he.data_bits[data_i].position;

        assert(remains_data || remains_check);

        if(!remains_data)
        {
            std::cout << RED << he.check_bits[check_i].value;
            check_i++;
            continue;
        }
        if(!remains_check)
        {
            std::cout << RESET << he.data_bits[data_i].value;
            data_i++;
            continue;
        }

        if(check_position < data_position)
        {
            std::cout << RED << he.check_bits[check_i].value;
            check_i++;
            continue;
        }
        else if(data_position < check_position)
        {
            std::cout << RESET << he.data_bits[data_i].value;
            data_i++;
            continue;
        }
        else
        {
            assert(false);
        }
    }
}

unsigned No_Parity_Bits(unsigned no_data_bits)
{
    unsigned no_parity_bits = 1;
    while((1UL << no_parity_bits) < no_parity_bits + no_data_bits + 1)
        no_parity_bits++;
    return no_parity_bits;
}

bool Is_Power_Of_2(unsigned n)
{
    return n == 1 || (n % 2 == 0 && Is_Power_Of_2(n / 2));
}

std::vector<bool> Bits_From_Unsigned(unsigned n, unsigned no_bits)
{
    if(no_bits == 0)
    {
        std::vector<bool> bits; 
        if(n == 0)
            return bits;

        bits.push_back(n % 2);

        std::vector<bool> remaining_bits = Bits_From_Unsigned(n / 2, 0);
        //for(auto bit: remaining_bits)
        for(std::vector<bool>::iterator bit_ptr = remaining_bits.begin();
                bit_ptr != remaining_bits.end(); bit_ptr++)
            bits.push_back(*bit_ptr);

        return bits;
    }
    else
    {
        std::vector<bool> bits;
        for(unsigned i = 0; i < no_bits; i++)
        {
            bits.push_back(n % 2);
            n /= 2;
        }
        return bits;
    }
}
